# ndaal_YARA_NYC

The YARA checker cleans, validates, de-duplicates and merges YARA rules.

# Motivation 

The possibility of having duplicated &/or corrupt rules withing a respective YARA ruleset grows with the size of your ruleset. If you're an active YARA user, you already know that YARA does not accept duplicated rules, corrupt rules, incomplete rules or rules with private module imports. At ndaal, we've created a tool that will take care of all those potential points of failure for you. 

# Idea 

The YARA checker checks multiple aspects of a rule and takes each rule through a process of cleaning, validation and de-duplication. It does this with the help of the Plyara parser as well as the yara-python module. 

# Usage 

```
usage: yara_checker.py [-h] -p PATH [-dn DIRECTORY_NAME]

Deduplicate,validate and merge YARA Rules

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  Absolute path to YARA rules.
  -dn DIRECTORY_NAME, --directory_name DIRECTORY_NAME
                        When called, a directory will be created with your
                        specified name and all rules will be dumped into
                        separate files into that directory.
```

# Getting started 

- Make sure the target system at which you would like to run the YARA checker has python installed. Run python -V to check the installed python version. 

- Also make sure that you have the YARA checker's requirements available on your system or virtual environment. The requirements are available inside requirements.txt in this repository.

- Download the repo. and navigate to it. 

- Run the YARA checker with: python3 yara_checker.py -p path/to/rules_dir

- Review the results in your working directory. 


# Example Ruleset 

The example ruleset accumulated under the directory 'example' in this repository and used below is taken from https://github.com/Yara-Rules/rules, all rights belong to the authors of the project. Keep your dataset up to date by checking out their repository &/or populating your ruleset with your own rules.

# Example

```
$ python3 yara_checker.py -p example 
```

```
   __   __ _    ____      _       ____ _   _ _____ ____ _  _______ ____ 
   \ \ / // \  |  _ \    / \     / ___| | | | ____/ ___| |/ / ____|  _ \ 
    \ V // _ \ | |_) |  / _ \   | |   | |_| |  _|| |   | ' /|  _| | |_) |
     | |/ ___ \|  _ <  / ___ \  | |___|  _  | |__| |___| . \| |___|  _ < 
     |_/_/   \_\_| \_\/_/   \_\  \____|_| |_|_____\____|_|\_\_____|_| \_\ 
 
     Version 0.5.1, Alaa Jubakhanji
     ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG
 
2022-04-26 13:29:20.069604
Your YARA rules are being processed...
 
Summary:
[*] Process took: 0:00:03.657578
[*] 11833 Rules have been validated, deduplicated and merged!
[*] 0 Duplicated rules have been discarded
[*] 11833 Deduplicated and valid rules now reside in your merged file.

Happy hunting!

```

# Output 

After running, you will find in your working directory a file named merged_rules_<run_date>.yar with all your merged rules. You'll also find a file named error_log.txt, this file includes all errors, if any, the checker encountered in rule files while validating them using the yara-python module. Consequently, those files are discarded.

# Future releases 

We will work actively on enhancing this tool and keeping it up to date. We are also open to adding any features you would like to see in future releases; if you have any suggestions, please make them! 


## Scorecard

Aggregate score: 5.7 / 10

Check scores:


|  SCORE  |        NAME         |             REASON             |                                                                         DETAILS                                                                          |                                             DOCUMENTATION/REMEDIATION                                              |
|---------|---------------------|--------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| 10 / 10 | Binary-Artifacts | no binaries found in the repo |  | https://github.com/ossf/scorecard/blob/7ed886f1bd917d19cb9d6ce6c10e80e81fa31c39/docs/checks.md#binary-artifacts |
| 0 / 10 | Code-Review | found 20 unreviewed changesets out of 20 -- score normalized to 0 |  | https://github.com/ossf/scorecard/blob/7ed886f1bd917d19cb9d6ce6c10e80e81fa31c39/docs/checks.md#code-review |
| |   | 0 |   | / |   | 1 | 0 |   | | |   | C | o | d | e | - | R | e | v | i | e | w |   | | |   | f | o | u | n | d |   | 2 | 0 |   | u | n | r | e | v | i | e | w | e | d |   | c | h | a | n | g | e | s | e | t | s |   | o | u | t |   | o | f |   | 2 | 0 |   | - | - |   | s | c | o | r | e |   | n | o | r | m | a | l | i | z | e | d |   | t | o |   | 0 |   | | |   |   | | |   | h | t | t | p | s | : | / | / | g | i | t | h | u | b | . | c | o | m | / | o | s | s | f | / | s | c | o | r | e | c | a | r | d | / | b | l | o | b | / | 7 | e | d | 8 | 8 | 6 | f | 1 | b | d | 9 | 1 | 7 | d | 1 | 9 | c | b | 9 | d | 6 | c | e | 6 | c | 1 | 0 | e | 8 | 0 | e | 8 | 1 | f | a | 3 | 1 | c | 3 | 9 | / | d | o | c | s | / | c | h | e | c | k | s | . | m | d | # | c | o | d | e | - | r | e | v | i | e | w |   | |
| |   | | |   |   |   | | |   | 0 |   | | |   |   |   | | |   | / |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | C |   | | |   | o |   | | |   | d |   | | |   | e |   | | |   | - |   | | |   | R |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | f |   | | |   | o |   | | |   | u |   | | |   | n |   | | |   | d |   | | |   |   |   | | |   | 2 |   | | |   | 0 |   | | |   |   |   | | |   | u |   | | |   | n |   | | |   | r |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | c |   | | |   | h |   | | |   | a |   | | |   | n |   | | |   | g |   | | |   | e |   | | |   | s |   | | |   | e |   | | |   | t |   | | |   | s |   | | |   |   |   | | |   | o |   | | |   | u |   | | |   | t |   | | |   |   |   | | |   | o |   | | |   | f |   | | |   |   |   | | |   | 2 |   | | |   | 0 |   | | |   |   |   | | |   | - |   | | |   | - |   | | |   |   |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   |   |   | | |   | n |   | | |   | o |   | | |   | r |   | | |   | m |   | | |   | a |   | | |   | l |   | | |   | i |   | | |   | z |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | t |   | | |   | o |   | | |   |   |   | | |   | 0 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | h |   | | |   | t |   | | |   | t |   | | |   | p |   | | |   | s |   | | |   | : |   | | |   | / |   | | |   | / |   | | |   | g |   | | |   | i |   | | |   | t |   | | |   | h |   | | |   | u |   | | |   | b |   | | |   | . |   | | |   | c |   | | |   | o |   | | |   | m |   | | |   | / |   | | |   | o |   | | |   | s |   | | |   | s |   | | |   | f |   | | |   | / |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | a |   | | |   | r |   | | |   | d |   | | |   | / |   | | |   | b |   | | |   | l |   | | |   | o |   | | |   | b |   | | |   | / |   | | |   | 7 |   | | |   | e |   | | |   | d |   | | |   | 8 |   | | |   | 8 |   | | |   | 6 |   | | |   | f |   | | |   | 1 |   | | |   | b |   | | |   | d |   | | |   | 9 |   | | |   | 1 |   | | |   | 7 |   | | |   | d |   | | |   | 1 |   | | |   | 9 |   | | |   | c |   | | |   | b |   | | |   | 9 |   | | |   | d |   | | |   | 6 |   | | |   | c |   | | |   | e |   | | |   | 6 |   | | |   | c |   | | |   | 1 |   | | |   | 0 |   | | |   | e |   | | |   | 8 |   | | |   | 0 |   | | |   | e |   | | |   | 8 |   | | |   | 1 |   | | |   | f |   | | |   | a |   | | |   | 3 |   | | |   | 1 |   | | |   | c |   | | |   | 3 |   | | |   | 9 |   | | |   | / |   | | |   | d |   | | |   | o |   | | |   | c |   | | |   | s |   | | |   | / |   | | |   | c |   | | |   | h |   | | |   | e |   | | |   | c |   | | |   | k |   | | |   | s |   | | |   | . |   | | |   | m |   | | |   | d |   | | |   | # |   | | |   | c |   | | |   | o |   | | |   | d |   | | |   | e |   | | |   | - |   | | |   | r |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   |   |   | | |   | |
| 0 / 10 | License | license file not detected |  | https://github.com/ossf/scorecard/blob/7ed886f1bd917d19cb9d6ce6c10e80e81fa31c39/docs/checks.md#license |
| 10 / 10 | Pinned-Dependencies | all dependencies are pinned | Info: GitHub-owned GitHubActions are pinned Info: Third-party GitHubActions are pinned Info: Dockerfile dependencies are pinned Info: no insecure (not pinned by hash) dependency downloads found in Dockerfiles Info: no insecure (not pinned by hash) dependency downloads found in shell scripts Info: Pip installs are pinned Info: npm installs are pinned | https://github.com/ossf/scorecard/blob/7ed886f1bd917d19cb9d6ce6c10e80e81fa31c39/docs/checks.md#pinned-dependencies |
| 0 / 10 | Security-Policy | security policy file not detected | Warn: no security policy file detected: On GitHub: Enable private vulnerability disclosure in your repository settings https://docs.github.com/en/code-security/security-advisories/repository-security-advisories/configuring-private-vulnerability-reporting-for-a-repository Add a section in your SECURITY.md indicating you have enabled private reporting, and tell them to follow the steps in https://docs.github.com/en/code-security/security-advisories/guidance-on-reporting-and-writing/privately-reporting-a-security-vulnerability to report vulnerabilities. On GitLab: Add a section in your SECURITY.md indicating the process to disclose vulnerabilities for your project. Examples: https://github.com/ossf/scorecard/blob/main/SECURITY.md, https://github.com/slsa-framework/slsa-github-generator/blob/main/SECURITY.md, https://github.com/sigstore/.github/blob/main/SECURITY.md. For additional information on vulnerability disclosure, see https://github.com/ossf/oss-vulnerability-guide/blob/main/maintainer-guide.md. (Medium effort) Warn: no security file to analyze: On GitHub: Enable private vulnerability disclosure in your repository settings https://docs.github.com/en/code-security/security-advisories/repository-security-advisories/configuring-private-vulnerability-reporting-for-a-repository Add a section in your SECURITY.md indicating you have enabled private reporting, and tell them to follow the steps in https://docs.github.com/en/code-security/security-advisories/guidance-on-reporting-and-writing/privately-reporting-a-security-vulnerability to report vulnerabilities. On GitLab: Provide a point of contact in your SECURITY.md. Examples: https://github.com/ossf/scorecard/blob/main/SECURITY.md, https://github.com/slsa-framework/slsa-github-generator/blob/main/SECURITY.md, https://github.com/sigstore/.github/blob/main/SECURITY.md. (Low effort) Warn: no security file to analyze: On GitHub: Enable private vulnerability disclosure in your repository settings https://docs.github.com/en/code-security/security-advisories/repository-security-advisories/configuring-private-vulnerability-reporting-for-a-repository Add a section in your SECURITY.md indicating you have enabled private reporting, and tell them to follow the steps in https://docs.github.com/en/code-security/security-advisories/guidance-on-reporting-and-writing/privately-reporting-a-security-vulnerability to report vulnerabilities. On GitLab: Add a section in your SECURITY.md indicating the process to disclose vulnerabilities for your project. Examples: https://github.com/ossf/scorecard/blob/main/SECURITY.md, https://github.com/slsa-framework/slsa-github-generator/blob/main/SECURITY.md, https://github.com/sigstore/.github/blob/main/SECURITY.md. (Low effort) Warn: no security file to analyze: On GitHub: Enable private vulnerability disclosure in your repository settings https://docs.github.com/en/code-security/security-advisories/repository-security-advisories/configuring-private-vulnerability-reporting-for-a-repository Add a section in your SECURITY.md indicating you have enabled private reporting, and tell them to follow the steps in https://docs.github.com/en/code-security/security-advisories/guidance-on-reporting-and-writing/privately-reporting-a-security-vulnerability to report vulnerabilities. On GitLab: Add a section in your SECURITY.md indicating the process to disclose vulnerabilities for your project. Examples: https://github.com/ossf/scorecard/blob/main/SECURITY.md, https://github.com/slsa-framework/slsa-github-generator/blob/main/SECURITY.md, https://github.com/sigstore/.github/blob/main/SECURITY.md. (Low effort) | https://github.com/ossf/scorecard/blob/7ed886f1bd917d19cb9d6ce6c10e80e81fa31c39/docs/checks.md#security-policy |
| 10 / 10 | Vulnerabilities | no vulnerabilities detected |  | https://github.com/ossf/scorecard/blob/7ed886f1bd917d19cb9d6ce6c10e80e81fa31c39/docs/checks.md#vulnerabilities |

