#!/usr/bin/env python
# coding: utf-8
"""The YARA checker cleans, validates, de-duplicates and merges YARA rules."""
import sys
import os
import time
import argparse
from pathlib import Path
from fnmatch import fnmatch
from datetime import datetime
import yara
import plyara
import pandas as pd
from plyara import utils

__version__ = "0.5.1"
__author__ = "Alaa Jubakhanji"
__organization__ = "ndaal Gesellschaft für Sicherheit in der " \
    "Informationstechnik mbH & Co KG"

parser = argparse.ArgumentParser(
    description='Deduplicate,validate and merge YARA Rules')
parser.add_argument('-p', '--path', type=str,
                    help='Absolute path to YARA rules.', required=True)
parser.add_argument('-dn', '--directory_name', type=str,
                    help='When called, a directory will be created with '
                         'your specified name and all rules will be dumped '
                         'into separate files into that directory.',
                         required=False)

args = parser.parse_args()
abs_path = args.path
dir_name = args.directory_name

def merge_separatly(rules_dir: str) -> None:
    """Merge rules into seperate files."""
    try:
        Path(rules_dir).mkdir(parents=True, exist_ok=False)
    except FileExistsError:
        print(f"{rules_dir} already exists.")
    else:
        print(f"{rules_dir} has been created")

    count = 0

    for rule in range(len(df["rule_name"])):
        with open(
            rules_dir + "/" + str(rule) + ".yara", "w+", encoding="utf-8"
        ) as outfile:
            rebuild = utils.rebuild_yara_rule(rules_to_dict[count])
            outfile.write(rebuild)
            count += 1

if __name__ == "__main__":
    start_time = datetime.now()

    if args.path:
        if not os.path.exists(args.path):
            sys.exit(
                f"[!] {args.path} does not exist. Please provide a valid path to"
                " your YARA rules!")

    print(" ")
    print(" ")
    print("   __   __ _    ____      _  "
        "     ____ _   _ _____ ____ _  _______ ____ ")

    print("   \\ \\ / // \\  |  _ \\    / \\  "
        "   / ___| | | | ____/ ___| |/ / ____|  _ \\ ")

    print("    \\ V // _ \\ | |_) |  / _ \\  "
        " | |   | |_| |  _|| |   | ' /|  _| | |_) |")
    print("     | |/ ___ \\|  _ <  / ___ \\  "
        "| |___|  _  | |__| |___| . \\| |___|  _ < ")
    print("     |_/_/   \\_\\_| \\_\\/_/   \\_\\  "
        "\\____|_| |_|_____\\____|_|\\_\\_____|_| \\_\\ ")
    print(" ")
    print(f"     Version {__version__}")
    print(f"     {__author__}")
    print(f"     {__organization__}")
    print(" ")
    print(start_time)
    print("Your YARA rules are being processed...")
    print(" ")

    parsed_rules = []
    yara_parser = plyara.Plyara()

    yara_rules = []
    pattern = ["*.yar", "*.yara"]
    for path, subdirs, files in os.walk(abs_path):
        for name in files:
            for p in pattern:
                if fnmatch(name, p):
                    yara_rules.append(os.path.join(path, name))

    error = []
    encodings = [
        "utf-8",
        "cp1252",
        "windows-1250",
        "windows-1252",
        "ascii",
        "ISO-8859-1",
        "GB2312",
        "latin-1",
        "utf-16",
    ]

    for rule_file in yara_rules:
        for enc in encodings:
            try:
                with open(rule_file, encoding=enc) as fh:
                    fh.readlines()
                    fh.seek(0)
            except (UnicodeDecodeError, UnicodeError):
                error.append(rule_file)
            else:
                break

    valid_yara = [x for x in yara_rules if x not in error]

    androguard = []
    for rule_file in valid_yara:
        with open(rule_file, encoding="utf-8") as myfile:
            if "androguard" in myfile.read():
                androguard.append(rule_file)

    include = []
    for rule_file in valid_yara:
        with open(rule_file, encoding="utf-8") as myfile:
            if "include" in myfile.read():
                include.append(rule_file)

    incompilable = []
    with open("error_log.txt", "w+", encoding="utf-8") as log:
        for rule_file in valid_yara:
            try:
                yara_rule = yara.compile(
                    filepath=rule_file
                )
            except yara.SyntaxError as syn_e:
                log.write(str(syn_e) + "\n")
                incompilable.append(rule_file)
            continue

    invalid = androguard + include + incompilable
    valid = [rule_file for rule_file in valid_yara if rule_file not in invalid]

    parser_error = []

    for yara_rule in valid:
        with open(yara_rule, encoding="utf-8") as fh:
            try:
                yara_rules = yara_parser.parse_string(fh.read())
                parsed_rules += yara_rules
                yara_parser.clear()
            except plyara.exceptions.ParseTypeError:
                parser_error.append(yara_rule)
            continue

    df = pd.DataFrame(parsed_rules)
    df = df.loc[df.astype(str).drop_duplicates(
        subset=['rule_name'], keep='first').index]
    df = df.reset_index(drop=True)
    df.fillna('', inplace=True)
    rules_to_dict = df.to_dict(orient='records')

    timestr = time.strftime("%Y%m%d")

    with open("merged_rules_" + timestr + ".yara", "w+", encoding="utf-8") as out_file:
        for x in rules_to_dict:
            rebuilt_rule = utils.rebuild_yara_rule(x)
            out_file.write(rebuilt_rule)

    if args.directory_name:
        merge_separatly(dir_name)

    end_time = datetime.now()

    print(" ")
    print("Summary:")
    print(f"[*] Process took: {end_time - start_time}")
    print(
        f"[*] {len(parsed_rules)} Rules have been validated, "
        "deduplicated and merged!"
    )
    print(
        f"[*] {len(parsed_rules) - len(rules_to_dict)} Duplicated rules have been "
        "discarded.")
    print(
        f"[*] {len(rules_to_dict)} Deduplicated and valid rules now "
        "reside in your merged file."
    )
    print("")
    print("Happy hunting!")
    print("")
